package com.spring.upwork.model;

import java.util.Collection;

public class Category {
    private Long uid;
    private String label;
    private Collection<Speciality> specialities;
}
