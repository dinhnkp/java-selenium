package com.main.model.test.constant;

public interface EndPoint {
    String INDEX = "users";
    String TYPE = "_doc";
}
