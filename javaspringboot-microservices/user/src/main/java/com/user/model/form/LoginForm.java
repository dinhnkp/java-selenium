package com.user.model.form;

public interface LoginForm {
    public void setUsername(String username);
    public String getUsername();

    public void setPassword(String password);
    public String getPassword();
}