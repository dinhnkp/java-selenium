import undetected_chromedriver as uc
import json
import time
import sys
import requests

# from selenium import webdriver
from pprint import pformat


def main():
    console = sys.stdout

    options = uc.ChromeOptions()
    # options.add_argument("--ignore-certificate-error")
    # options.add_argument("--ignore-ssl-errors")

    driver = uc.Chrome(version_main=98, enable_cdp_events=True)
    contain = "https://www.upwork.com/search/jobs/url"

    # https://www.upwork.com/ab/jobs/search/url?initial_request=true&location=Japan&payment_verified=1&per_page=10&sort=recency&t=&user_location_match=2
    jp = "https://www.upwork.com/ab/jobs/search/url"
    # c0 = "https://www.upwork.com/search/jobs/url?per_page=10&sort=recency"
    # c1 = "https://www.upwork.com/search/jobs/url?per_page=10&sort=recency&page=1"
    # c2 = "https://www.upwork.com/search/jobs/url?per_page=10&sort=recency&page=2"

    class Object:
        def toJSON(self):
            return json.dumps(self, default = lambda o: o.__dict__, sort_keys=True, indent=4)

    # driver.get_log()

    # def printEventData(eventdata):
    #     print(pformat(eventdata))



    def printMessage(message):
        print(pformat(message))


    def h(css):
        try:
            driver.find_element_by_css_selector(css).click()
            return True
        except: 
            return False
    
    def clickIfExist(a,button):
        while True:
            if (h(a)):
                break
            if (h(button)):
                break


    # run()
    def postMessage(url,message):
        x = requests.post(url,message);
        print(x.text)

        # s = Session()
        # req = Request("POST", LOCMOJI_API, data=message)
        # prepared = req.prepare()
        # res = s.send(prepared)
        # if res.status_code == 200:
        #     return res.text
        # return ""
    

    def getResponseBody(response):
        url = response['params']['response']['url']
        # print(url)
        if jp in url:
        # if url == c0 or url == c1 or url == c2:
            requestId = response['params']["requestId"] 
            body = driver.execute_cdp_cmd("Network.getResponseBody", {'requestId': requestId})['body']
            job_obj = json.dumps(json.loads(body))
            # print(job_obj + ",")
            # print(job_obj)
            postMessage("http://localhost:8081/api/v1/upwork/detect",job_obj)
    
    def googleLogin():
        driver.get('https://accounts.google.com/servicelogin')
        driver.find_element_by_id("identifierId").send_keys('dinhnkp@aladintech.co')
        driver.find_element_by_xpath("//button[@class='VfPpkd-LgbsSe VfPpkd-LgbsSe-OWXEXe-k8QpJ VfPpkd-LgbsSe-OWXEXe-dgl2Hf nCP5yc AjY5Oe DuMIQc qfvgSe qIypjc TrZEUc lw1w4b']").click()
        time.sleep(5)
        driver.find_element_by_css_selector("input[name='password']").send_keys('so12345678')
        driver.find_element_by_xpath("//button[@class='VfPpkd-LgbsSe VfPpkd-LgbsSe-OWXEXe-k8QpJ VfPpkd-LgbsSe-OWXEXe-dgl2Hf nCP5yc AjY5Oe DuMIQc qfvgSe qIypjc TrZEUc lw1w4b']").click()

    def run():
        # driver.get("https://www.upwork.com/search/jobs/?page=2&sort=recency")
        a1 = "a[ng-click=\"selectPage(page - 1, 'previous', $event)\"]"
        a2 = "a[ng-click=\"selectPage(page + 1, 'next', $event)\"]"
        button1 = "li:nth-child(2) button:nth-child(1)"
        button2 = "li:nth-child(9) button:nth-child(1)"

        # driver.add_cdp_listener('Network.requestWillBeSent', printMessage)
        # driver.add_cdp_listener('Network.dataReceived', printMessage)

        driver.add_cdp_listener('Network.responseReceived',getResponseBody)

        count = 0
        # time.sleep(30);
        try:
            while True:
                with open("data/"+str(count)+'.json','w') as f:
                    count = count + 1
                    sys.stdout = f

                    driver.get("https://www.upwork.com/search/jobs/?page=2&sort=recency")
                    time.sleep(20)
                    # clickIfExist(a1,button1)
                    # clickIfExist(a2,button2)
                    time.sleep(20)

        except KeyboardInterrupt:
            driver.quit()
            sys.stdout = console 

    def run1():
        driver.add_cdp_listener('Network.responseReceived',getResponseBody)
        try:
            googleLogin()
            time.sleep(60)
            while True:
                driver.get('https://www.upwork.com/nx/jobs/search/?sort=recency&user_location_match=2&payment_verified=1&location=Japan')
                time.sleep(300)
        except KeyboardInterrupt:
            driver.quit()

    run1()



if __name__ == '__main__':
    main()
# 
# driver.get("https://www.upwork.com/search/jobs/?sort=recency&payment_verified=1&location=Japan")

# # sleep(10)