package com.main.model.test.constant;

public enum Gender {
    MALE,
    FEMALE,
    UNKNOWN
}
