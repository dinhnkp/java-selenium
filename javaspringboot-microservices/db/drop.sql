USE db_university;

DROP TABLE IF EXISTS hibernate_sequence;
DROP TABLE IF EXISTS user_authorities;
DROP TABLE IF EXISTS student;

DROP TABLE IF EXISTS authority;
DROP TABLE IF EXISTS course;

DROP TABLE IF EXISTS country;
DROP TABLE IF EXISTS region;

DROP TABLE IF EXISTS user;